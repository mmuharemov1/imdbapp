package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentZanrovi extends Fragment implements GlumacResultReceiver.Receiver {
    public ListView listaZanrova;
    public AdapterListeZanrova adapter;
    public List<Zanr> zanrovi;
    GlumacResultReceiver mReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_zanrovi,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(MainActivity.ID_GLUMCA!="" && !FragmentGlumci.unosTeksta.equals("actor:")) {
            Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ZanroviServis.class);
            mReceiver = new GlumacResultReceiver(new Handler());
            mReceiver.setReceiver(FragmentZanrovi.this);
            String s = "";
            try {
                s = URLEncoder.encode(MainActivity.ID_GLUMCA, "utf-8");
            } catch (UnsupportedEncodingException e) {
            }
            intent.putExtra("url", "http://api.themoviedb.org/3/discover/movie?api_key=bbb246edaa176fc0dceb1dbd90fb1333&with_cast=" + s + "&sort_by=primary_release_date.desc");
            intent.putExtra("receiver", mReceiver);
            getActivity().startService(intent);
        }else if(MainActivity.ID_GLUMCA!="" && FragmentGlumci.unosTeksta.equals("actor:"))
        {
            ArrayList<Zanr> results = MainActivity.db.dajZanrove(MainActivity.ID_GLUMCA);
            if(results.size()!=0) {
                zanrovi = results;
                listaZanrova = (ListView) getView().findViewById(R.id.listaZanrova);
                adapter = new AdapterListeZanrova(getActivity(), zanrovi);
                listaZanrova.setAdapter(adapter);
                listaZanrova.deferNotifyDataSetChanged();
            }
        }


    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch(resultCode){

            case 1:

                break;
            case 5:
                ArrayList<Zanr> results = resultData.getParcelableArrayList("result");
                zanrovi=results;
                try {

                    listaZanrova = (ListView)getView().findViewById(R.id.listaZanrova);;
                    adapter= new AdapterListeZanrova(getActivity(), zanrovi);
                    listaZanrova.setAdapter(adapter);
                    listaZanrova.deferNotifyDataSetChanged();
                }catch(Exception e){}
                break;
            case 2:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),"Greska",Toast.LENGTH_LONG).show();
                break;
        }
    }
}
