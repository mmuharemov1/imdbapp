package com.example.mirza.projektnizadatak2;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.LinkAddress;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mirza on 30.3.2017.
 */

public class Glumac implements Parcelable {
    private String id;
    private String Naziv;
    private String datumRodjenja, datumSmrti;
    private String bio,mjestoRodjenja,spol;
    private String slikaGlumca;
    private String link;
    private String rating;

    protected Glumac(Parcel in){
        this.id = in.readString();
        this.Naziv=in.readString();
        this.datumRodjenja = in.readString();
        this.datumSmrti = in.readString();
        this.bio = in.readString();
        this.mjestoRodjenja = in.readString();
        this.spol = in.readString();
        this.slikaGlumca = in.readString();
        this.link = in.readString();
        this.rating = in.readString();
    }

    public Glumac(String id, String naziv, String datumRodjenja, String datumSmrti, String bio, String mjestoRodjenja, String spol, String slikaGlumca, String link, String rating) {
        this.id = id;
        Naziv=naziv;
        this.datumRodjenja = datumRodjenja;
        this.datumSmrti = datumSmrti;
        this.bio = bio;
        this.mjestoRodjenja = mjestoRodjenja;
        this.spol = spol;
        this.slikaGlumca = slikaGlumca;
        this.link = link;
        this.rating = rating;
    }

    public static final Creator<Glumac> CREATOR =  new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel in) {
            return new Glumac(in);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNaziv() {
        return Naziv;
    }

    public void setNaziv(String naziv) {
        Naziv = naziv;
    }

    public String getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(String datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getDatumSmrti() {
        return datumSmrti;
    }

    public void setDatumSmrti(String datumSmrti) {
        this.datumSmrti = datumSmrti;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getMjestoRodjenja() {
        return mjestoRodjenja;
    }

    public void setMjestoRodjenja(String mjestoRodjenja) {
        this.mjestoRodjenja = mjestoRodjenja;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public String getSlikaGlumca() {
        return slikaGlumca;
    }

    public void setSlikaGlumca(String slikaGlumca) {
        this.slikaGlumca = slikaGlumca;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bio);
        dest.writeString(datumRodjenja);
        dest.writeString(datumSmrti);
        dest.writeString(Naziv);
        dest.writeString(id);
        dest.writeString(mjestoRodjenja); dest.writeString(spol);
        dest.writeString(slikaGlumca);
        dest.writeString(link); dest.writeString(rating);

    }
}
