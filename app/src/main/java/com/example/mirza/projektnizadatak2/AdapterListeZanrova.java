package com.example.mirza.projektnizadatak2;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

/**
 */

public class AdapterListeZanrova extends BaseAdapter {
    private Context mContext;
    private List<Zanr> lista;

    public AdapterListeZanrova(Context mContext, List<Zanr> lista) {
        this.mContext = mContext;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View clanListe = View.inflate(mContext,R.layout.element_liste_zanrova,null);
        ImageView slicica = (ImageView)clanListe.findViewById(R.id.icon2);
        TextView tekst =(TextView)clanListe.findViewById(R.id.Itemname);
        slicica.setImageResource(lista.get(position).getSlika());
        tekst.setText(lista.get(position).getNaziv());

        clanListe.setTag(lista.get(position).getId());
        return clanListe;
    }
}
