package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentReziseri extends Fragment implements GlumacResultReceiver.Receiver{
    public ArrayList<String> reziseri;
    public GlumacResultReceiver mReceiver;
    ListView listaRezisera;
    ArrayList<String> reziseriLista;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reziseri,container,false);


        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Toast.makeText(getActivity(),MainActivity.ID_GLUMCA,Toast.LENGTH_LONG).show();

        if(MainActivity.ID_GLUMCA!="" && !FragmentGlumci.unosTeksta.equals("actor:")) {
            Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ReziserServis.class);
            mReceiver = new GlumacResultReceiver(new Handler());
            mReceiver.setReceiver(FragmentReziseri.this);
            String id= null;
            try {
                id = URLEncoder.encode(MainActivity.ID_GLUMCA, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            intent.putExtra("url", "http://api.themoviedb.org/3/discover/movie?api_key=bbb246edaa176fc0dceb1dbd90fb1333&with_cast=" + id + "&sort_by=primary_release_date.desc");
            intent.putExtra("receiver", mReceiver);
            getActivity().startService(intent);
        }else if(MainActivity.ID_GLUMCA!="" && FragmentGlumci.unosTeksta.equals("actor:"))
        {
            ArrayList<String> reziseri = MainActivity.db.dajRezisere(MainActivity.ID_GLUMCA);
            if(reziseri.size()!=0) {
                reziseriLista = reziseri;
                listaRezisera = (ListView) getView().findViewById(R.id.listaR);
                final ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, reziseriLista);
                listaRezisera.setAdapter(adapter);
            }
        }

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch(resultCode){
            case 1:
                break;
            case 10:
                ArrayList<String> reziseri = resultData.getStringArrayList("result");


                try {
                    Log.d("proba", String.valueOf(reziseri.size()));
                    reziseriLista=reziseri;
                    listaRezisera=(ListView) getView().findViewById(R.id.listaR);
                    final ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,reziseriLista);
                    listaRezisera.setAdapter(adapter);

                }catch (Exception e){}
                break;
            case 2:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),"Greska",Toast.LENGTH_LONG).show();
                break;
        }
    }

}
