package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentBiografija extends Fragment implements GlumacResultReceiver.Receiver{
    GlumacResultReceiver mReceiver2;
    Glumac pristigli=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_biografija,container,false);

        WindowManager wm = getActivity().getWindowManager();
        Display d = wm.getDefaultDisplay();
        if (d.getWidth() > d.getHeight()) {
            if(MainActivity.glumci2.size()!=0)
            {
                pristigli=MainActivity.glumci2.get(0);

                ImageView slikaGlumca = (ImageView)v.findViewById(R.id.imageView2);
                TextView imeiprezime = (TextView)v.findViewById(R.id.Imeiprezime2);
                TextView godine = (TextView)v.findViewById(R.id.Datumi);
                TextView mjestoR = (TextView)v.findViewById(R.id.Mjesto2);
                TextView spol = (TextView)v.findViewById(R.id.Spol2);
                TextView linkich = (TextView)v.findViewById(R.id.Link2);
                TextView biobio = (TextView)v.findViewById(R.id.Bio2);

                if(pristigli.getSpol().equals("Male")) v.setBackgroundColor(getResources().getColor(R.color.muska));
                else v.setBackgroundColor(getResources().getColor(R.color.zenska));
                imeiprezime.setText(pristigli.getNaziv());

                Picasso.with(getActivity().getApplicationContext()).load(pristigli.getSlikaGlumca()).into(slikaGlumca);

                if(pristigli.getDatumSmrti().equals("null"))
                {
                    godine.setText(String.valueOf(pristigli.getDatumRodjenja()));
                } else godine.setText(String.valueOf(pristigli.getDatumRodjenja())+" - "+String.valueOf(pristigli.getDatumSmrti()));

                mjestoR.setText(pristigli.getMjestoRodjenja());
                spol.setText(pristigli.getSpol());
                linkich.setText("IMDB: "+pristigli.getLink());
                biobio.setText(pristigli.getBio());

            }

        }else {

            if(FragmentGlumci.unosTeksta.equals("actor:"))
            {
                pristigli=MainActivity.db.DajBiografiju(MainActivity.ID_GLUMCA);
            }else {
                Bundle bundle = getArguments();
                pristigli = (Glumac) bundle.getParcelable("glumac");
            }

        ImageView slikaGlumca = (ImageView)v.findViewById(R.id.imageView2);
        TextView imeiprezime = (TextView)v.findViewById(R.id.Imeiprezime2);
        TextView godine = (TextView)v.findViewById(R.id.Datumi);
        TextView mjestoR = (TextView)v.findViewById(R.id.Mjesto2);
        TextView spol = (TextView)v.findViewById(R.id.Spol2);
        TextView linkich = (TextView)v.findViewById(R.id.Link2);
        TextView biobio = (TextView)v.findViewById(R.id.Bio2);

        if(pristigli.getSpol().equals("Male")) v.setBackgroundColor(getResources().getColor(R.color.muska));
        else v.setBackgroundColor(getResources().getColor(R.color.zenska));
        imeiprezime.setText(pristigli.getNaziv());

            Picasso.with(getActivity().getApplicationContext()).load(pristigli.getSlikaGlumca()).into(slikaGlumca);


                godine.setText(String.valueOf(pristigli.getDatumRodjenja()));

        mjestoR.setText(pristigli.getMjestoRodjenja());
        spol.setText(pristigli.getSpol());
        linkich.setText("IMDB: "+pristigli.getLink());
        biobio.setText(pristigli.getBio());
        }

        Button bookmark = (Button)v.findViewById(R.id.bookmark);
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainActivity.db.DodajGlumca(pristigli))
                {
                    Toast.makeText(getActivity(),"Uspjesno unesen",Toast.LENGTH_LONG).show();
                    mReceiver2 = new GlumacResultReceiver(new Handler());
                    mReceiver2.setReceiver(FragmentBiografija.this);
                    String s = "";
                    try {
                        s = URLEncoder.encode(pristigli.getId(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                    }
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ZanroviServis.class);
                    intent.putExtra("url", "http://api.themoviedb.org/3/discover/movie?api_key=bbb246edaa176fc0dceb1dbd90fb1333&with_cast=" + s + "&sort_by=primary_release_date.desc");
                    intent.putExtra("receiver", mReceiver2);
                    getActivity().startService(intent);

                    Intent intentR = new Intent(Intent.ACTION_SYNC, null, getActivity(), ReziserServis.class);
                    intentR.putExtra("url", "http://api.themoviedb.org/3/discover/movie?api_key=bbb246edaa176fc0dceb1dbd90fb1333&with_cast=" + s + "&sort_by=primary_release_date.desc");
                    intentR.putExtra("receiver", mReceiver2);
                    getActivity().startService(intentR);


                }
                else Toast.makeText(getActivity(),"Vec je u bazi",Toast.LENGTH_LONG).show();


            }
        });

        Button brisanje = (Button)v.findViewById(R.id.obrisi);

        brisanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainActivity.db.obrisiGlumca(pristigli.getId()))
                {
                    Toast.makeText(getActivity(),"Uspjesno obrisan",Toast.LENGTH_LONG).show();
                }else Toast.makeText(getActivity(),"Dati glumac nije u bazi",Toast.LENGTH_LONG).show();

            }
        });


        return v;
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        /*super.onViewCreated(v, savedInstanceState);
        Bundle bundle = getArguments();
        pristigli=null;
        if (bundle != null) {

            pristigli = (Glumac) bundle.getParcelable("glumac");

        }else if(FragmentGlumci.unosTeksta.equals("actor:"))
        {
            pristigli = MainActivity.db.DajBiografiju(MainActivity.ID_GLUMCA);
        }

        if(pristigli!=null)
        {



            ImageView slikaGlumca = (ImageView)v.findViewById(R.id.imageView2);
            TextView imeiprezime = (TextView)v.findViewById(R.id.Imeiprezime2);
            TextView godine = (TextView)v.findViewById(R.id.Datumi);
            TextView mjestoR = (TextView)v.findViewById(R.id.Mjesto2);
            TextView spol = (TextView)v.findViewById(R.id.Spol2);
            TextView linkich = (TextView)v.findViewById(R.id.Link2);
            TextView biobio = (TextView)v.findViewById(R.id.Bio2);

            if(pristigli.getSpol().equals("Male")) {
                v.setBackgroundColor(getResources().getColor(R.color.muska));
                //Toast.makeText(getActivity(),"|"+pristigli.getSpol()+"|",Toast.LENGTH_LONG).show();
            }
            else v.setBackgroundColor(getResources().getColor(R.color.zenska));
            imeiprezime.setText(pristigli.getNaziv());
            Picasso.with(getActivity().getApplicationContext()).load(pristigli.getSlikaGlumca()).into(slikaGlumca);

            if(pristigli.getDatumSmrti().equals("null"))
            {
                godine.setText(String.valueOf(pristigli.getDatumRodjenja()));
            } else godine.setText(String.valueOf(pristigli.getDatumRodjenja())+" - "+String.valueOf(pristigli.getDatumSmrti()));

            mjestoR.setText(pristigli.getMjestoRodjenja());
            spol.setText(pristigli.getSpol());
            linkich.setText("IMDB: "+pristigli.getLink());
            biobio.setText(pristigli.getBio());
        }
        */
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch(resultCode){
            case 1:
                break;
            case 5:
                ArrayList<Zanr> results;
                try {
                    results = resultData.getParcelableArrayList("result");
                    MainActivity.db.DodajZanrove(results,pristigli.getId());

                }catch(Exception e){}
                break;
            case 10:
                ArrayList<String> resultsR;
                try {
                    resultsR = resultData.getStringArrayList("result");
                    Log.d("resultsR",String.valueOf(resultsR.size()));
                    MainActivity.db.DodajRezisere(resultsR,pristigli.getId());

                    //MainActivity.db.DodajZanrove(results,pristigli.getId());

                }catch(Exception e){}
                break;
            case 2:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),"greska",Toast.LENGTH_LONG).show();
                break;
        }
    }

}


