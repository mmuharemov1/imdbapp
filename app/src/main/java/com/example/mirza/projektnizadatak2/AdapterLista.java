package com.example.mirza.projektnizadatak2;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;
import com.squareup.picasso.Picasso;

/**
 */

public class AdapterLista extends BaseAdapter {
    private Context mContext;
    private List<Glumac> lista;

    public AdapterLista(Context mContext, List<Glumac> lista) {
        this.mContext = mContext;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View clanListe = View.inflate(mContext,R.layout.element_liste,null);
        ImageView slika= (ImageView) clanListe.findViewById(R.id.icon);
        TextView ime = (TextView)clanListe.findViewById(R.id.Imeiprezime);
        TextView godina = (TextView)clanListe.findViewById(R.id.Godina);
        TextView grad = (TextView)clanListe.findViewById(R.id.Grad);
        TextView rate = (TextView)clanListe.findViewById(R.id.Rate);

        ime.setText(lista.get(position).getNaziv());
        godina.setText(String.valueOf(lista.get(position).getDatumRodjenja()));
        grad.setText(lista.get(position).getMjestoRodjenja());
        Picasso.with(mContext).load(lista.get(position).getSlikaGlumca()).into(slika);
        rate.setText(String.valueOf(lista.get(position).getRating()));

        clanListe.setTag(lista.get(position).getId());
        return clanListe;
    }
}
