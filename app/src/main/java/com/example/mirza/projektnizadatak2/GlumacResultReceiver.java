package com.example.mirza.projektnizadatak2;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by Mirza on 6/10/2017.
 */

public class GlumacResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public GlumacResultReceiver(Handler handler){
        super(handler);
    }

    public void setReceiver(Receiver receiver ){
        mReceiver=receiver;
    }

    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData){
        if(mReceiver!= null){
            mReceiver.onReceiveResult(resultCode,resultData);
        }
    }
}
