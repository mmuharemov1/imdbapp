package com.example.mirza.projektnizadatak2;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FragmentRezervacija.MemorisiKlikClick {

    public static String ID_GLUMCA="";
    public static List<Glumac> glumci2=new ArrayList<Glumac>();
    public static DatabaseHelper db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        db=new DatabaseHelper(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);


        WindowManager wm = getWindowManager();
        Display d = wm.getDefaultDisplay();
        if (d.getWidth() > d.getHeight()) {

            getFragmentManager().beginTransaction().replace(R.id.leftFrame,new FragmentGlumci()).addToBackStack(null).commit();
            getFragmentManager().beginTransaction().replace(R.id.rightFrame,new FragmentBiografija()).addToBackStack(null).commit();

            Button glumciLand = (Button) findViewById(R.id.glumciwButton);
            Button otherLand = (Button) findViewById(R.id.otherButton);

            glumciLand.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    getFragmentManager().beginTransaction().replace(R.id.leftFrame,new FragmentGlumci()).addToBackStack(null).commit();
                    getFragmentManager().beginTransaction().replace(R.id.rightFrame,new FragmentBiografija()).addToBackStack(null).commit();
                }
            });

            otherLand.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    getFragmentManager().beginTransaction().replace(R.id.leftFrame,new FragmentReziseri()).addToBackStack(null).commit();
                    getFragmentManager().beginTransaction().replace(R.id.rightFrame,new FragmentZanrovi()).addToBackStack(null).commit();
                }
            });

        } else {

            android.app.FragmentManager FM = getFragmentManager();
            FragmentTransaction FT = FM.beginTransaction();
            FragmentMeni fragmentMeni = new FragmentMeni();
            FT.add(R.id.meni, fragmentMeni);
            FT.commit();

            FragmentTransaction FT2 = FM.beginTransaction();
            FragmentGlumci fragmentGlumci = new FragmentGlumci();
            FT2.add(R.id.liste, fragmentGlumci);
            FT2.commit();
        }
    }

    @Override
    public void RememberMeClick(int godina, int mjesec, int dan, String tekst, String film) {
        long startMillis = 0;
        long endMillis = 0;
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int hour = Calendar.getInstance().get(Calendar.HOUR);
        int min = Calendar.getInstance().get(Calendar.MINUTE);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(year,month,day,hour,min);
        Calendar endTime = Calendar.getInstance();
        endTime.set(godina, mjesec, dan, hour, min);


        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, film)
                .putExtra(CalendarContract.Events.DESCRIPTION, tekst)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "mirza_muharemovic@live.com");
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.closeDB();
    }
}

