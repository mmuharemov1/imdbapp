package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class FragmentRezervacija extends Fragment {

    private MemorisiKlikClick zc;
    private static String filmNaziv;
    private Button zapamti;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Bundle bundle = getArguments();
        String pristigli="";
        if (bundle != null) pristigli = bundle.getString("film");
        filmNaziv=pristigli;
        final View view = inflater.inflate(R.layout.forma,container,false);
        LinearLayout rootElement = (LinearLayout) view.findViewById(R.id.formaLayout);
        zapamti = (Button) view.findViewById(R.id.zapamti_btn);
        try {
            zc = (MemorisiKlikClick) getActivity();
        } catch (ClassCastException e) {

        }


        TextView tekst = (TextView)view.findViewById(R.id.film);
        EditText edittekst = (EditText) view.findViewById(R.id.editfilm);
        DatePicker simpleDatePicker = (DatePicker) view.findViewById(R.id.datumMeni);
        tekst.setText(filmNaziv);

        final int g = simpleDatePicker.getYear();  final int m = simpleDatePicker.getMonth();
        final int d = simpleDatePicker.getDayOfMonth(); final String f = getNaziv_filma();
        final String t = edittekst.getText().toString();


        zapamti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zc.RememberMeClick(g,m,d,t,f);
            }
        });



        return view;
    }

    public static String getNaziv_filma() {
        return filmNaziv;
    }


    public interface MemorisiKlikClick{
        public void RememberMeClick(int g, int m, int d, String txt, String f);


    }



}