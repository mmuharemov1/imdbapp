package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentFilmovi extends Fragment implements GlumacResultReceiver.Receiver {

    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=0;
    public static int STATUS_ERROR=2;

    public ArrayList<String> glumci;
    public ListView listaGlumaca;
    public AdapterLista adapter;

    Button pretraga;
    EditText unos;
    GlumacResultReceiver mReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filmovi, container, false);

        Button dugme = (Button)v.findViewById(R.id.pretragaFilmovi);
        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText e = (EditText) getView().findViewById(R.id.filmoviUnosTeksta);


                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), FilmoviServisi.class);
                    mReceiver = new GlumacResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentFilmovi.this);
                    intent.putExtra("url", "https://api.themoviedb.org/3/search/movie?api_key=bbb246edaa176fc0dceb1dbd90fb1333&query="+e.getText());
                    intent.putExtra("receiver", mReceiver);
                    getActivity().startService(intent);


            }
        });



        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumci);
        AdapterLista aa = new AdapterLista(getActivity(),glumci2);
        listaGlumaca.setAdapter(aa);

        /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ID_GLUMCA=glumci.get(position).getID();
                oic.onItemClicked(glumci.get(position));
            }
        });*/
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch(resultCode){
            case 1:
                break;
            case 10:
                ArrayList<String> results = resultData.getStringArrayList("result");
                glumci=results;
                try {
                    listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumciFilmovi);
                    final ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,glumci);
                    listaGlumaca.setAdapter(adapter);

                    listaGlumaca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String zaSlanje;

                            zaSlanje = glumci.get(position);
                            Bundle bundle = new Bundle();
                            bundle.putString("film",zaSlanje);
                            FragmentRezervacija fb=new FragmentRezervacija();
                            fb.setArguments(bundle);
                            WindowManager wm = getActivity().getWindowManager();
                            Display d = wm.getDefaultDisplay();
                            if (d.getWidth() > d.getHeight()) {
                                getFragmentManager().beginTransaction().replace(R.id.leftFrame,fb).addToBackStack(null).commit();
                            }else getFragmentManager().beginTransaction().replace(R.id.liste,fb).addToBackStack(null).commit();

                        }
                    });
                }catch (Exception e){}
                break;
            case 2:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),"greska",Toast.LENGTH_LONG).show();
                break;
        }
    }
}
