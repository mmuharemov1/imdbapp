package com.example.mirza.projektnizadatak2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.BoringLayout;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ReziserServis extends IntentService {
    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=10;
    public static int STATUS_ERROR=2;

    public ReziserServis(){
        super(null);
    }

    public ReziserServis(String name){
        super(name);
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        Bundle bundle = new Bundle();

        if(!TextUtils.isEmpty(url)) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            try {
                ArrayList<String> rez;
                rez = uzmiPodatke(url);
                if (rez.size() > 0 && rez!=null) {
                    bundle.putStringArrayList("result",rez);
                    receiver.send(STATUS_FINISHED, bundle);

                }

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);

            }
        }
    }

    public ArrayList<String> uzmiPodatke(String urlPretrage) {
        ArrayList<String> rez;
        try {

            URL url = new URL(urlPretrage);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            String rezultat = "";

            while ((line = bufferedReader.readLine()) != null) {
                rezultat += line;
            }
            if (in != null) {
                in.close();
            }

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("results");
            ArrayList<String> reziseri = new ArrayList<String>();
            ArrayList<String> nazivi=new ArrayList<String>();

            for(int i=0; i < items.length(); i++){
                JSONObject jedanFilm = items.getJSONObject(i);
                String id_filma=jedanFilm.getString("id");
                id_filma = URLEncoder.encode(id_filma, "utf-8");
                URL url1 = new URL("https://api.themoviedb.org/3/movie/"+id_filma+"?api_key=bbb246edaa176fc0dceb1dbd90fb1333&append_to_response=credits");
                HttpURLConnection urlConnection1 = (HttpURLConnection) url1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(in1));
                String line1 = "";
                String rezultat1 = "";
                while ((line1 = bufferedReader1.readLine()) != null) {
                    rezultat1 += line1;
                }

                JSONObject jo1 = new JSONObject(rezultat1);
                JSONObject items1 = jo1.getJSONObject("credits");
                JSONArray crew = items1.getJSONArray("crew");

                for(int j=0;j<crew.length(); j++) {
                    JSONObject reziser = crew.getJSONObject(j);
                    Boolean nadjen=false;
                    if(reziser.getString("job").equals("Director"))
                    {
                        for(int z=0;z<reziseri.size();z++)
                            if(reziseri.get(z).equals(reziser.getString("name"))) nadjen=true;
                        if(!nadjen) {
                            reziseri.add(reziser.getString("name"));
                            break;
                        }
                    }

                }
                if(reziseri.size()==7) break;

            }


            rez=reziseri;
            return rez;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (JSONException e){
            e.printStackTrace();
        }


        return null;
    }

}
