package com.example.mirza.projektnizadatak2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Mirza on 6/14/2017.
 */

public class ZanroviServis extends IntentService {
    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=5;
    public static int STATUS_ERROR=2;

    public ZanroviServis(){
        super(null);
    }

    public ZanroviServis(String name){
        super(name);
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        Bundle bundle = new Bundle();

        if(!TextUtils.isEmpty(url)) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            try {
                ArrayList<Zanr> rez;
                rez = uzmiPodatke(url);
                Log.d("velicina",String.valueOf(rez.size()));
               if (rez.size() > 0 && rez!=null) {
                    bundle.putParcelableArrayList("result",rez);
                    receiver.send(STATUS_FINISHED, bundle);

                }

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);

            }
        }
    }

    public ArrayList<Zanr> uzmiPodatke(String urlPretrage) {
        ArrayList<Zanr> rez=new ArrayList<Zanr>();
       try {
            URL url = new URL("https://api.themoviedb.org/3/genre/movie/list?api_key=bbb246edaa176fc0dceb1dbd90fb1333");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            String rezultat = "";
            while ((line = bufferedReader.readLine()) != null) {
                rezultat += line;
            }

            ArrayList<Zanr> zanroviLista=new ArrayList<Zanr>();
            JSONObject jo1 = new JSONObject(rezultat);
            JSONArray items = jo1.getJSONArray("genres");

            for(int i=0; i < items.length(); i++) {
                JSONObject zanr=items.getJSONObject(i);
                zanroviLista.add(new Zanr(zanr.getString("name"),R.drawable.drama,zanr.getInt("id")));

            }

            URL url1 = new URL(urlPretrage);
            HttpURLConnection urlConnection1 = (HttpURLConnection) url1.openConnection();
            InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());

            BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(in1));
            String line1 = "";
            String rezultat1 = "";

            while ((line1 = bufferedReader1.readLine()) != null) {
                rezultat1 += line1;
            }
            if (in1 != null) {
                in1.close();
            }

            JSONObject jo = new JSONObject(rezultat1);
            JSONArray items1 = jo.getJSONArray("results");
            Log.d("items1",String.valueOf(items1.length()));
            ArrayList<Zanr> zanroviKonacna = new ArrayList<Zanr>();

            for(int i=0; i < items1.length(); i++){
                JSONObject filmic = items1.getJSONObject(i);

                JSONArray id_zanrova = filmic.getJSONArray("genre_ids");
                String z=new String();

                if(id_zanrova.length()!=0) z = id_zanrova.getString(0);
                Log.d("z",z);

                for (Zanr p: zanroviLista) {
                    if(String.valueOf(p.getId()).equals(z))
                    {
                        Boolean nadjen=false;
                        for(int t=0;t<zanroviKonacna.size();t++)
                            if(zanroviKonacna.get(t).getNaziv().equals(p.getNaziv())) nadjen=true;
                        if(!nadjen) {zanroviKonacna.add(new Zanr(p.getNaziv(),p.getSlika(),p.getId())); break;}
                    }
                }

                if(zanroviKonacna.size()==7) break;

            }



            rez=zanroviKonacna;
            return rez;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (JSONException e){
            e.printStackTrace();
        }


        return null;
    }

}
