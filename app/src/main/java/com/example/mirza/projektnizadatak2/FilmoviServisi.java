package com.example.mirza.projektnizadatak2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.BoringLayout;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class FilmoviServisi extends IntentService {
    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=10;
    public static int STATUS_ERROR=2;

    public FilmoviServisi(){
        super(null);
    }

    public FilmoviServisi(String name){
        super(name);
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        Bundle bundle = new Bundle();

        if(!TextUtils.isEmpty(url)) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            try {
                ArrayList<String> rez;
                rez = uzmiPodatke(url);
                if (rez.size() > 0 && rez!=null) {
                    bundle.putStringArrayList("result",rez);
                    receiver.send(STATUS_FINISHED, bundle);

                }

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);

            }
        }
    }

    public ArrayList<String> uzmiPodatke(String urlPretrage) {
        ArrayList<String> rez=new ArrayList<String>();
        try {

            URL url = new URL(urlPretrage);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            String rezultat = "";

            while ((line = bufferedReader.readLine()) != null) {
                rezultat += line;
            }
            if (in != null) {
                in.close();
            }

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("results");
            for(int i=0;i<items.length();i++)
            {
                JSONObject film=items.getJSONObject(i);
                String nazivFilma=film.getString("title");
                rez.add(nazivFilma);
            }
            return rez;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (JSONException e){
            e.printStackTrace();
        }


        return null;
    }

}
