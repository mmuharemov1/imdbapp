package com.example.mirza.projektnizadatak2;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Mirza on 31.3.2017.
 */

public class Zanr implements Parcelable
{
    private String Naziv;
    private  int slika;
    private  int Id;

    public Zanr(String naziv, int slika, int id) {
        Naziv = naziv;
        this.slika = slika;
        Id = id;
    }

    protected Zanr(Parcel in){
        this.Naziv = in.readString();
        this.slika = in.readInt();
        this.Id=in.readInt();

    }

    public static final Creator<Zanr> CREATOR =  new Creator<Zanr>() {
        @Override
        public Zanr createFromParcel(Parcel in) {
            return new Zanr(in);
        }

        @Override
        public Zanr[] newArray(int size) {
            return new Zanr[size];
        }
    };


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNaziv() {
        return Naziv;
    }

    public void setNaziv(String naziv) {
        Naziv = naziv;
    }

    public int getSlika() {
        return slika;
    }

    public void setSlika(int slika) {
        this.slika = slika;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Naziv);
        dest.writeInt(slika);
        dest.writeInt(Id);
    }
}
