package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentGlumci extends Fragment implements GlumacResultReceiver.Receiver {

    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=0;
    public static int STATUS_ERROR=2;
    public static String unosTeksta="";
    private ListView listaGlumaca;
    public AdapterLista adapter;

    Button pretraga;
    EditText unos;
    GlumacResultReceiver mReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_glumci, container, false);

        listaGlumaca = (ListView) v.findViewById(R.id.listaGlumci);

        listaGlumaca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Glumac zaSlanje;

                zaSlanje = MainActivity.glumci2.get(position);
                MainActivity.ID_GLUMCA=zaSlanje.getId();

                Bundle bundle = new Bundle();
                bundle.putParcelable("glumac",zaSlanje);
                FragmentBiografija fb=new FragmentBiografija();
                fb.setArguments(bundle);
                WindowManager wm = getActivity().getWindowManager();
                Display d = wm.getDefaultDisplay();
                if (d.getWidth() > d.getHeight()) {
                    getFragmentManager().beginTransaction().replace(R.id.rightFrame,fb).addToBackStack(null).commit();
                }else getFragmentManager().beginTransaction().replace(R.id.liste,fb).addToBackStack(null).commit();

            }
        });

        if (MainActivity.glumci2.size() != 0)
        {

            AdapterLista aa = new AdapterLista(getActivity(), MainActivity.glumci2);
            listaGlumaca.setAdapter(aa);
            listaGlumaca.deferNotifyDataSetChanged();

        }

        final EditText ime = (EditText)v.findViewById(R.id.editText);
        Button dugme = (Button)v.findViewById(R.id.button69);
        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText e = (EditText) getView().findViewById(R.id.editText);

                if(e.getText().toString().indexOf("actor:")!=-1)
                {
                    MainActivity.glumci2=MainActivity.db.DajGlumce(e.getText().toString().substring(6));
                    listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumci);
                    AdapterLista aa = new AdapterLista(getActivity(), MainActivity.glumci2);
                    listaGlumaca.setAdapter(aa);
                    listaGlumaca.deferNotifyDataSetChanged();
                }else if(e.getText().toString().indexOf("director:")!=-1)
                {
                    MainActivity.glumci2=MainActivity.db.dajGlumcePrekoRezisera(e.getText().toString().substring(9));
                    listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumci);
                    AdapterLista aa = new AdapterLista(getActivity(), MainActivity.glumci2);
                    listaGlumaca.setAdapter(aa);
                    listaGlumaca.deferNotifyDataSetChanged();
                }
                else{
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), GlumacServis.class);
                    mReceiver = new GlumacResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentGlumci.this);
                    intent.putExtra("url", "https://api.themoviedb.org/3/search/person?api_key=bbb246edaa176fc0dceb1dbd90fb1333&query=" + e.getText() + "&include_adult=false");
                    intent.putExtra("receiver", mReceiver);
                    getActivity().startService(intent);
                }

            }
        });

        Button otvoriFF=(Button)v.findViewById(R.id.otvoriFF);

        otvoriFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentFilmovi ff=new FragmentFilmovi();
                WindowManager wm = getActivity().getWindowManager();
                Display d = wm.getDefaultDisplay();
                if (d.getWidth() > d.getHeight()) {
                    getFragmentManager().beginTransaction().replace(R.id.leftFrame,ff).addToBackStack(null).commit();
                }else getFragmentManager().beginTransaction().replace(R.id.liste,ff).addToBackStack(null).commit();
            }
        });



        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumci);
        AdapterLista aa = new AdapterLista(getActivity(),glumci2);
        listaGlumaca.setAdapter(aa);

        /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ID_GLUMCA=glumci.get(position).getID();
                oic.onItemClicked(glumci.get(position));
            }
        });*/
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch(resultCode){
            case 1:
                break;
            case 0:
                ArrayList<Glumac> results = resultData.getParcelableArrayList("result");
                MainActivity.glumci2=results;
                try {
                    listaGlumaca = (ListView) getView().findViewById(R.id.listaGlumci);
                    AdapterLista aa = new AdapterLista(getActivity(), MainActivity.glumci2);
                    listaGlumaca.setAdapter(aa);
                    listaGlumaca.deferNotifyDataSetChanged();

                    listaGlumaca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                           Glumac zaSlanje;

                            zaSlanje = MainActivity.glumci2.get(position);
                            MainActivity.ID_GLUMCA=zaSlanje.getId();

                            Bundle bundle = new Bundle();
                            bundle.putParcelable("glumac",zaSlanje);
                            FragmentBiografija fb=new FragmentBiografija();
                            fb.setArguments(bundle);
                            WindowManager wm = getActivity().getWindowManager();
                            Display d = wm.getDefaultDisplay();
                            if (d.getWidth() > d.getHeight()) {
                                getFragmentManager().beginTransaction().replace(R.id.rightFrame,fb).addToBackStack(null).commit();
                            }else getFragmentManager().beginTransaction().replace(R.id.liste,fb).addToBackStack(null).commit();

                        }
                    });
                }catch (Exception e){}
                break;
            case 2:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                    Toast.makeText(getActivity(),"Greska",Toast.LENGTH_LONG).show();
                break;
        }
    }
}
