package com.example.mirza.projektnizadatak2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.BoringLayout;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Mirza on 6/14/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "ProjektniZadatak.db";

    // Table Names
    private static final String TABLE_GLUMCI = "glumci";
    private static final String TABLE_REZISERI = "reziseri";
    private static final String TABLE_ZANROVI = "zanrovi";

    // Common column names
    private static final String KEY_ID = "_id";
    private static final String KEY_CREATED_AT = "created_at";

    // Tabela Glumci - column names
    //public static final String GLUMAC_ID="_id";
    public static final String GLUMAC_IME_I_PREZIME="ime";
    public static final String GLUMAC_DATUM_RODJENJA="datum_rodjenja";
    public static final String GLUMAC_DATUM_SMRTI="datum_smrti";
    public static final String GLUMAC_RATING="rating";
    public static final String GLUMAC_ID="glumac_id";
    public static final String GLUMAC_SLIKA="slika";
    public static final String GLUMAC_BIOGRAFIJA="biografija";
    public static final String GLUMAC_LINK="link";
    public static final String GLUMAC_MJESTO_RODJENJA="mjesto_rodjenja";
    public static final String GLUMAC_SPOL="spol";

    // Tabela Reziseri - column names
    //public static final String REZISER_ID="_id";
    public static final String REZISER_IME="ime";
    public static final String REZISER_ID_GLUMACA="id_glumaca";

    // Tabela Zanrovi - column names
    //public static final String ZANR_ID="_id";
    public static final String ZANR_NAZIV="zanr";
    public static final String ZANR_ID_GLUMACA="id_glumaca";

    // Table Create Statements
    // Kreiranje tabele Glumci
    public static final String CREATE_TABLE_GLUMCI="create table "+TABLE_GLUMCI+"("+KEY_ID+" integer primary key autoincrement ,"+
            GLUMAC_IME_I_PREZIME+" text , "+
            GLUMAC_DATUM_RODJENJA+" text , "+
            GLUMAC_DATUM_SMRTI+" text , "+
            GLUMAC_MJESTO_RODJENJA+" text , "+
            GLUMAC_ID+" text , "+
            GLUMAC_LINK+" text , "+
            GLUMAC_BIOGRAFIJA+" text , "+
            GLUMAC_SLIKA+" text , "+
            GLUMAC_RATING+" integer , "+
            GLUMAC_SPOL+" text );";

    // Tag table create statement
    public static final String CREATE_TABLE_REZISERI = "create table "+TABLE_REZISERI+"("+KEY_ID+" integer primary key autoincrement, "+
            REZISER_IME+" text not null, "+
            REZISER_ID_GLUMACA +" text not null);";

    // todo_tag table create statement
    public static final String CREATE_TABLE_ZANROVI = "CREATE TABLE "+TABLE_ZANROVI+"("+KEY_ID+" integer primary key autoincrement, "+
            ZANR_NAZIV+" text not null, "+
            ZANR_ID_GLUMACA+" text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_GLUMCI);
        db.execSQL(CREATE_TABLE_REZISERI);
        db.execSQL(CREATE_TABLE_ZANROVI);
    }

    public boolean DodajGlumca(Glumac glumac){
        boolean unesen=false;
        if(!JeLiUBazi(glumac)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues glumacBaza = new ContentValues();
            glumacBaza.put(GLUMAC_ID, glumac.getId());
            glumacBaza.put(GLUMAC_IME_I_PREZIME, glumac.getNaziv());
            glumacBaza.put(GLUMAC_BIOGRAFIJA, glumac.getBio());
            glumacBaza.put(GLUMAC_DATUM_RODJENJA, glumac.getDatumRodjenja());
            glumacBaza.put(GLUMAC_DATUM_SMRTI, glumac.getDatumSmrti());
            glumacBaza.put(GLUMAC_MJESTO_RODJENJA, glumac.getMjestoRodjenja());
            glumacBaza.put(GLUMAC_LINK, glumac.getLink());
            glumacBaza.put(GLUMAC_SLIKA, glumac.getSlikaGlumca());
            glumacBaza.put(GLUMAC_SPOL, glumac.getSpol());
            glumacBaza.put(GLUMAC_RATING, glumac.getRating());
            long result = db.insert(TABLE_GLUMCI, null, glumacBaza);
            if (result == -1) return false;
            unesen=true;
        }
        return unesen;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZANROVI);

        // create new tables
        onCreate(db);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public Boolean JeLiUBazi(Glumac glumac)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_GLUMCI + " WHERE "
                + GLUMAC_ID + " = " + glumac.getId();

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.getCount()!=0) return true; else return false;
    }

    public void DodajZanrove(ArrayList<Zanr> listaZanrova, String id)
    {
        for(int i=0;i<listaZanrova.size();i++)

        {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT "+ZANR_ID_GLUMACA+" FROM " + TABLE_ZANROVI + " WHERE "
                    + ZANR_NAZIV + " LIKE " +"'"+ listaZanrova.get(i).getNaziv()+"'";
            Log.e(LOG, selectQuery);

            Cursor c = db.rawQuery(selectQuery, null);


            if(c.getCount()!=0)
            {
                c.moveToFirst();
                ContentValues values = new ContentValues();
                String lista =c.getString(c.getColumnIndex(ZANR_ID_GLUMACA))+id+" ";

                // updating row
                /*String s=listaZanrova.get(i).getNaziv();*/
                String kveri="UPDATE " + TABLE_ZANROVI+" SET "+ZANR_ID_GLUMACA+" = "+"'"+lista+"'"+" WHERE "+ZANR_NAZIV+" LIKE "+"'"+listaZanrova.get(i).getNaziv()+"'"+";";
                db.execSQL(kveri);
            }else
            {
                SQLiteDatabase db1 = this.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(ZANR_NAZIV,listaZanrova.get(i).getNaziv());
                values.put(ZANR_ID_GLUMACA, " "+id+" ");

                // updating row
                String s=listaZanrova.get(i).getNaziv();
                long result = db1.insert(TABLE_ZANROVI, null, values);
            }

        }
    }

    public void DodajRezisere(ArrayList<String> listaRezisera, String id)
    {

        for(int i=0;i<listaRezisera.size();i++)

        {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT "+REZISER_ID_GLUMACA+" FROM " + TABLE_REZISERI + " WHERE "
                    + REZISER_IME + " LIKE " +"'"+ listaRezisera.get(i)+"';";
            Log.e(LOG, selectQuery);

            Cursor c = db.rawQuery(selectQuery, null);


            if(c.getCount()!=0)
            {
                c.moveToFirst();
                ContentValues values = new ContentValues();
                String lista =c.getString(c.getColumnIndex(REZISER_ID_GLUMACA))+id+" ";

                // updating row
                /*String s=listaZanrova.get(i).getNaziv();*/
                String kveri="UPDATE " + TABLE_REZISERI+" SET "+REZISER_ID_GLUMACA+" = "+"'"+lista+"'"+" WHERE "+REZISER_IME+" LIKE "+"'"+listaRezisera.get(i)+"'"+";";
                db.execSQL(kveri);
            }else
            {
                SQLiteDatabase db1 = this.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(REZISER_IME,listaRezisera.get(i));
                values.put(REZISER_ID_GLUMACA, " "+id+" ");

                // updating row
                String s=listaRezisera.get(i);
                long result = db1.insert(TABLE_REZISERI, null, values);
            }

        }
    }

    public ArrayList<Glumac> DajGlumce(String s)
    {
        s=s.toUpperCase();
        ArrayList<Glumac> glumci = new ArrayList<Glumac>();
        String selectQuery = "SELECT * FROM " + TABLE_GLUMCI+" WHERE UPPER("+GLUMAC_IME_I_PREZIME+ ") LIKE "+"'%"+s+"%';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                String naziv = c.getString(c.getColumnIndex(GLUMAC_IME_I_PREZIME));
                String datumR = c.getString(c.getColumnIndex(GLUMAC_DATUM_RODJENJA));
                String datumS = c.getString(c.getColumnIndex(GLUMAC_DATUM_SMRTI));
                String rating = c.getString(c.getColumnIndex(GLUMAC_RATING));
                String id_glumca = c.getString(c.getColumnIndex(GLUMAC_ID));
                String slika = c.getString(c.getColumnIndex(GLUMAC_SLIKA));
                String biografija = c.getString(c.getColumnIndex(GLUMAC_BIOGRAFIJA));
                String link= c.getString(c.getColumnIndex(GLUMAC_LINK));
                String mjestoR = c.getString(c.getColumnIndex(GLUMAC_MJESTO_RODJENJA));
                String spol = c.getString(c.getColumnIndex(GLUMAC_SPOL));

                Glumac m = new Glumac(id_glumca,naziv,datumR,datumS,biografija,mjestoR,spol,slika,link,rating);


                glumci.add(m);
            } while (c.moveToNext());
        }

        return glumci;
    }

    public Glumac DajBiografiju(String id)
    {
        Glumac g=null;

        String selectQuery = "SELECT * FROM " + TABLE_GLUMCI+" WHERE "+GLUMAC_ID +" LIKE "+"'"+id+"';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {


                String naziv = c.getString(c.getColumnIndex(GLUMAC_IME_I_PREZIME));
                String datumR = c.getString(c.getColumnIndex(GLUMAC_DATUM_RODJENJA));
                String datumS = c.getString(c.getColumnIndex(GLUMAC_DATUM_SMRTI));
                String rating = c.getString(c.getColumnIndex(GLUMAC_RATING));
                String id_glumca = c.getString(c.getColumnIndex(GLUMAC_ID));
                String slika = c.getString(c.getColumnIndex(GLUMAC_SLIKA));
                String biografija = c.getString(c.getColumnIndex(GLUMAC_BIOGRAFIJA));
                String link= c.getString(c.getColumnIndex(GLUMAC_LINK));
                String mjestoR = c.getString(c.getColumnIndex(GLUMAC_MJESTO_RODJENJA));
                String spol = c.getString(c.getColumnIndex(GLUMAC_SPOL));

                Glumac m = new Glumac(id_glumca,naziv,datumR,datumS,biografija,mjestoR,spol,slika,link,rating);

                g=m;
        }



        return g;
    }

    public ArrayList<String> dajRezisere(String id)
    {
        ArrayList<String> reziseri = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_REZISERI+" WHERE "+REZISER_ID_GLUMACA+ " LIKE "+"'% "+id+" %';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                String naziv = c.getString(c.getColumnIndex(REZISER_IME));


                reziseri.add(naziv);
            } while (c.moveToNext());
        }
        Log.e(LOG,String.valueOf(reziseri.size()));

        return reziseri;
    }

    public ArrayList<Zanr> dajZanrove(String id)
    {
        ArrayList<Zanr> zanrovi = new ArrayList<Zanr>();
        String selectQuery = "SELECT * FROM " + TABLE_ZANROVI+" WHERE "+ZANR_ID_GLUMACA+ " LIKE "+"'% "+id+" %';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                String naziv = c.getString(c.getColumnIndex(ZANR_NAZIV));
                Zanr z = new Zanr(naziv,R.drawable.drama,0);

                zanrovi.add(z);
            } while (c.moveToNext());
        }
        Log.e(LOG,String.valueOf(zanrovi.size()));

        return zanrovi;
    }

    public ArrayList<Glumac> dajGlumcePrekoRezisera(String imeRezisera) {
        imeRezisera = imeRezisera.toUpperCase();
        ArrayList<Glumac> glumci = new ArrayList<Glumac>();
        ArrayList<String> ids = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_REZISERI + " WHERE UPPER(" + REZISER_IME + ") LIKE " + "'%" + imeRezisera + "%';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                String lista = c.getString(c.getColumnIndex(REZISER_ID_GLUMACA));
                int pocetak = 1, kraj = 1;
                while (pocetak != lista.length()-1) {
                    while(lista.charAt(kraj)!=' ') kraj++;
                    String jedanId=lista.substring(pocetak,kraj);
                    Boolean nadjen=false;
                    for(String s:ids)
                    {
                        if(ids.equals(jedanId)) nadjen=true;
                    }
                    if(!nadjen) ids.add(jedanId);
                    if(kraj!=lista.length()-1) {
                        kraj += 1;
                        pocetak = kraj;
                    }else
                    {
                        pocetak=kraj;
                    }
                }


            } while (c.moveToNext());
        }


        for(String s:ids)
        {

            selectQuery = "SELECT * FROM " + TABLE_GLUMCI+" WHERE "+GLUMAC_ID +" LIKE "+"'"+s+"';";

            Log.e(LOG, selectQuery);

            //SQLiteDatabase db = this.getReadableDatabase();
            c = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (c.moveToFirst()) {


                String naziv = c.getString(c.getColumnIndex(GLUMAC_IME_I_PREZIME));
                String datumR = c.getString(c.getColumnIndex(GLUMAC_DATUM_RODJENJA));
                String datumS = c.getString(c.getColumnIndex(GLUMAC_DATUM_SMRTI));
                String rating = c.getString(c.getColumnIndex(GLUMAC_RATING));
                String id_glumca = c.getString(c.getColumnIndex(GLUMAC_ID));
                String slika = c.getString(c.getColumnIndex(GLUMAC_SLIKA));
                String biografija = c.getString(c.getColumnIndex(GLUMAC_BIOGRAFIJA));
                String link= c.getString(c.getColumnIndex(GLUMAC_LINK));
                String mjestoR = c.getString(c.getColumnIndex(GLUMAC_MJESTO_RODJENJA));
                String spol = c.getString(c.getColumnIndex(GLUMAC_SPOL));

                Glumac m = new Glumac(id_glumca,naziv,datumR,datumS,biografija,mjestoR,spol,slika,link,rating);

                glumci.add(m);
            }
        }

        return glumci;
    }

    public Boolean obrisiGlumca(String id)
    {
        Boolean nadjenUBazi=false;
        String selectQuery = "SELECT * FROM " + TABLE_REZISERI+" WHERE "+REZISER_ID_GLUMACA+ " LIKE "+"'% "+id+" %';";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            nadjenUBazi=true;
            do {
                String lista = c.getString(c.getColumnIndex(REZISER_ID_GLUMACA));
                String naziv = c.getString(c.getColumnIndex(REZISER_IME));
                ArrayList<String> ids = new ArrayList<String>();
                int pocetak = 1, kraj = 1;
                while (pocetak != lista.length()-1) {
                    while(lista.charAt(kraj)!=' ') kraj++;
                    String jedanId=lista.substring(pocetak,kraj);
                    Boolean nadjen=false;
                    for(String s:ids)
                    {
                        if(ids.equals(jedanId)) nadjen=true;
                    }
                    if(!nadjen) ids.add(jedanId);
                    if(kraj!=lista.length()-1) {
                        kraj += 1;
                        pocetak = kraj;
                    }else
                    {
                        pocetak=kraj;
                    }
                }

                if(ids.size()==1)
                {
                    String brisanjeQ="DELETE FROM "+TABLE_REZISERI+" WHERE "+REZISER_IME+" LIKE "+"'"+naziv+"';";
                    db.execSQL(brisanjeQ);
                }else
                {
                    ids.remove(id);
                    String novaLista="";
                    for(int i=0;i<ids.size();i++)
                    {
                        if(i==0) novaLista+=" "+ids.get(i)+" ";
                        else novaLista+=ids.get(i)+" ";
                    }

                    String kveri="UPDATE " + TABLE_REZISERI+" SET "+REZISER_ID_GLUMACA+" = "+"'"+novaLista+"'"+" WHERE "+REZISER_IME+" LIKE "+"'"+naziv+"'"+";";
                    db.execSQL(kveri);


                }


            } while (c.moveToNext());
        }

        selectQuery = "SELECT * FROM " + TABLE_ZANROVI+" WHERE "+ZANR_ID_GLUMACA+ " LIKE "+"'% "+id+" %';";

        Log.e(LOG, selectQuery);

        db = this.getReadableDatabase();
        c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            nadjenUBazi=true;
            do {
                String lista = c.getString(c.getColumnIndex(ZANR_ID_GLUMACA));
                String naziv = c.getString(c.getColumnIndex(ZANR_NAZIV));
                ArrayList<String> ids = new ArrayList<String>();
                int pocetak = 1, kraj = 1;
                while (pocetak != lista.length()-1) {
                    while(lista.charAt(kraj)!=' ') kraj++;
                    String jedanId=lista.substring(pocetak,kraj);
                    Boolean nadjen=false;
                    for(String s:ids)
                    {
                        if(ids.equals(jedanId)) nadjen=true;
                    }
                    if(!nadjen) ids.add(jedanId);
                    if(kraj!=lista.length()-1) {
                        kraj += 1;
                        pocetak = kraj;
                    }else
                    {
                        pocetak=kraj;
                    }
                }

                if(ids.size()==1)
                {
                    String brisanjeQ="DELETE FROM "+TABLE_ZANROVI+" WHERE "+ZANR_NAZIV+" LIKE "+"'"+naziv+"';";
                    db.execSQL(brisanjeQ);
                }else
                {
                    ids.remove(id);
                    String novaLista="";
                    for(int i=0;i<ids.size();i++)
                    {
                        if(i==0) novaLista+=" "+ids.get(i)+" ";
                        else novaLista+=ids.get(i)+" ";
                    }

                    String kveri="UPDATE " + TABLE_ZANROVI+" SET "+ZANR_ID_GLUMACA+" = "+"'"+novaLista+"'"+" WHERE "+ZANR_NAZIV+" LIKE "+"'"+naziv+"'"+";";
                    db.execSQL(kveri);


                }


            } while (c.moveToNext());
        }

        if(nadjenUBazi)
        {
            String brisanjeQ="DELETE FROM "+TABLE_GLUMCI+" WHERE "+GLUMAC_ID+" LIKE "+"'"+id+"';";
            db.execSQL(brisanjeQ);
        }

        return nadjenUBazi;

    }





    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
