package com.example.mirza.projektnizadatak2;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by Mirza on 16.4.2017.
 */

public class FragmentMeni extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_meni,container,false);
        ImageButton zastavica=(ImageButton)v.findViewById(R.id.imageButton);

        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;



        ImageButton IB = (ImageButton)v.findViewById(R.id.imageButton);
        if(Locale.getDefault().getDisplayLanguage().equals("English"))
            IB.setImageResource(R.drawable.uk);
        else
            IB.setImageResource(R.drawable.bosna);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button glumciDugme = (Button) view.findViewById(R.id.dugmeG);
        Button zanroviDugme = (Button) view.findViewById(R.id.dugmeZ);
        Button reziseriDugme = (Button) view.findViewById(R.id.dugmeR);

        glumciDugme.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.liste,new FragmentGlumci()).addToBackStack(null).commit();
            }
        });

        reziseriDugme.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.liste,new FragmentReziseri()).addToBackStack(null).commit();
            }
        });

        zanroviDugme.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.liste,new FragmentZanrovi()).addToBackStack(null).commit();
            }
        });

    }


}
