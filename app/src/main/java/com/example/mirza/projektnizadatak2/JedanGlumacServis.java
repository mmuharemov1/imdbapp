package com.example.mirza.projektnizadatak2;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Mirza on 6/10/2017.
 */

public class JedanGlumacServis extends IntentService {
    public static int STATUS_RUNNING=1;
    public static int STATUS_FINISHED=0;
    public static int STATUS_ERROR=2;

    public JedanGlumacServis(){
        super(null);
    }

    public JedanGlumacServis(String name){
        super(name);
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        Bundle bundle = new Bundle();

        if(!TextUtils.isEmpty(url)) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            try {
                Glumac rez;
                rez = uzmiPodatkeGlumac(url);
                if (rez!=null) {
                    bundle.putParcelable("result",rez);
                    receiver.send(STATUS_FINISHED, bundle);

                }

            } catch (Exception e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);
            }
        }
    }

    public Glumac uzmiPodatkeGlumac(String urlPretrage) {

        Glumac rez=new Glumac("123","123","123","123","123","123","123","https://scontent.fbeg2-1.fna.fbcdn.net/v/t1.0-9/16681661_10208947001615314_8413374500351949232_n.jpg?oh=8a8e90474f83f01ace4d7cd91f908b45&oe=599E2CEC","123","123");
        /*try {
            URL url = new URL(urlPretrage);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            String rezultat = "";

            while ((line = bufferedReader.readLine()) != null) {
                rezultat += line;
            }
            if (in != null) {
                in.close();
            }

            JSONObject jedanGlumac = new JSONObject(rezultat);


                String id=jedanGlumac.getString("id");
                String name=jedanGlumac.getString("name");
                Double rating = jedanGlumac.getDouble("popularity");
                DecimalFormat df = new DecimalFormat("#.##");
                String r=df.format(rating);

                String datum_rodjenja=jedanGlumac.getString("birthday");
                String datum_smrti=jedanGlumac.getString("deathday");
                String spol=jedanGlumac.getString("gender");
                if(spol=="1") spol="Female";
                else spol="Male";
                String biografija=jedanGlumac.getString("biography");
                String mjesto_rodjenja=jedanGlumac.getString("place_of_birth");
                String imdb_link="http://www.imdb.com/name/"+jedanGlumac.getString("imdb_id");
                String slika = "https://image.tmdb.org/t/p/w500/"+jedanGlumac.getString("profile_path");

                Glumac m = new Glumac(id,name,datum_rodjenja,datum_smrti,biografija,mjesto_rodjenja,spol,slika,imdb_link,r);

            rez=m;
            return rez;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (JSONException e){
            e.printStackTrace();
        }

                */
        return rez;
    }


}
